﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTween;

public class Main : MonoBehaviour
{

    public PTweenPlayerComponent target;

    void Start()
    {

    }

    void Update()
    {
        PTweenUtil.Update();
        if (Input.GetKeyDown(KeyCode.Q))
            PTweenUtil.StartPTweenPlayerComponent(target, PTweenAnimationDirection.ANIMATE_FORWARD);
        if (Input.GetKeyDown(KeyCode.E))
            PTweenUtil.StartPTweenPlayerComponent(target, PTweenAnimationDirection.ANIMATE_BACKWARD);
    }
}
